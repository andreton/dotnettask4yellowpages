﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading;

namespace DotNetTask3
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person1 = new Person("Andreas", "Myhr"); //Different person objects from different backgrounds :) 
            Person person2 = new Person("Darth", "Vader");
            Person person3 = new Person("Frodo", "Baggins");
            Person person4 = new Person("Harry", "Potter");
            Person person5 = new Person("Steven", "Gerrard");

            Dictionary<string, Person> username = new Dictionary<string, Person>(); //Using a dictionary for searching because of constant time complexity
            username.Add("AndreasMyhr", person1);   //My idea here is to use the fullname without whitespace as the key. This will make it easy and fast to search for part of or the full names
            username.Add("DarthVader", person2); // I would override the add method so that the persons full name is stripped of white space and used for the key in the dictionary
            username.Add("FrodoBaggins", person3);
            username.Add("HarryPotter", person4);
            username.Add("StevenGerrard", person5);


            //Main functionality starts
            do
            {
                searchForName(getUserInput(), username); //A search-function takes in the user input function as a parameter along with the dictionary
                Console.WriteLine("Press enter to restart the application, any other key will exit");
            }
            while (Console.ReadKey().Key == ConsoleKey.Enter);
        }
        public static string getUserInput()
        {
            Console.WriteLine("Write in the name or part of the name that you are searching for");
            string search;
            while (true) //User is stuck in loop until criteria for string is reached, only letters
            {
                search = Console.ReadLine();
                if (Regex.IsMatch(search, @"^[a-zA-ZøæåØÆÅ\s]+$"))
                { //Checking if user misspells or uses some wrong number/symbols
                    break;
                }
                else
                {
                    Console.WriteLine("You can only use letters");
                }
            }
            return search;
        }

        static void searchForName(string input, Dictionary<string,Person> keyName)
            {
                string foundPersons = ""; //String for adding all found persons 
                foreach (KeyValuePair<string, Person> item in keyName) //Foreach to loop through Dictionary
                {
                    input = Regex.Replace(input, @"\s+", "").ToLower(); //search is created as lower case with no whitespace, Andreas Myhr becomes andreasmyhr
                    string keyLower = item.Key.ToLower();//The key in directonary is also converted to all lowercase
                    if (String.Equals(input, keyLower))
                    {
                        foundPersons+=("First name: " + ((item.Value as Person).firstName) + " Last name: " + ((item.Value as Person).lastName)+"\n"); //If the name matches exactly you are given full info
                    }
                    else if (keyLower.Contains(input))
                    { //Adds all found persons to a string
                        foundPersons += "First name: " + ((item.Value as Person).firstName) + " Last name: " + ((item.Value as Person).lastName+"\n");
                    }
                }

                if (foundPersons == "") //If the string is empty, no persons were found
                {
                    Console.WriteLine("There were no persons who matched your entry, please try again");
                }
                else{ //Else the rest of the persons are written out
                    Console.WriteLine(foundPersons);
                }


            }






        }
        

     
    
  
}
